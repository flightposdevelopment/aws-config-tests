#!/bin/bash

. ../bash/colours.sh   

APP_NAME=ecs-cli-demo

echo
echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
echo -e ${BOLD}${BLUE} +++ ECS CONFIGURE +++${NC}
echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
echo

echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
echo Checking AWS CONFIG

if [[ -z "$AWS_ACCESS_KEY_ID" ]] ; then
    echo "AWS keys not set."
    . ../bash/get-aws-profile.sh    
fi

#ecs-cli configure --cluster sandbox --region eu-west-2 --cfn-stack-name sandbox-gordian --default-launch-type FARGATE 


echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
echo Configuring $APP_NAME

# --------------------------------------------------------------------------------
# Create a cluster configuration
#
#	Note use --default-launch-type FARGATE or a role and other values will be needed if using EC2.
#
echo -e "\tCreate a cluster configuration"
ecs-cli configure --cluster $APP_NAME \
					--region $AWS_REGION \
					--default-launch-type FARGATE \
					--config-name $APP_NAME \
					--cfn-stack-name $APP_NAME

echo -e "\tCreate a APP_NAME using access key and secret key"

ecs-cli configure profile --access-key $AWS_ACCESS_KEY_ID --secret-key $AWS_SECRET_ACCESS_KEY --profile-name $APP_NAME

echo -e "\tSets the cluster $APP_NAME configuration to be read from by default."
ecs-cli configure default --config-name $APP_NAME


echo -e "\tRun:"
echo -e "\t ${BOLD}ecs-cli up${NORM} \t-- to configure the cluster"
echo -e "\t ${BOLD}ecs-cli down${NORM} \t-- to destroy the cluster"

echo 
echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
echo -e "\ndone"


