AWSTemplateFormatVersion: '2010-09-09'
Description: Deploy a service on AWS Fargate, hosted in a public subnet, and accessible via a public load balancer.
Parameters:

  VpcStackName:
    Type: String
    Description: The name of the parent Fargate networking stack that you created. Necessary
                 to locate and reference resources created by that stack.

  StackName:
      Description: Please enter the name for this VPC
      Type: String
      Default: unknown
      
  StackStage:
      Description: Please enter the stage for this VPC
      Type: String
      Default: unknown
                 
  ServiceName:
    Type: String
    Default: GenericService
    Description: A name for the service in the cluster.
    
  ImageUrl:
    Type: String
    Default: nginx
    Description: The url of a docker image that contains the application process that
                 will handle the traffic for this service.
                 Images in the Docker Hub registry are available by default. Other repositories are specified with either repository-url/image:tag or repository-url/image@digest.
                 Images in Amazon ECR repositories can be specified by either using the full registry/repository:tag or registry/repository@digest. For example, 012345678910.dkr.ecr.<region-name>.amazonaws.com/<repository-name>:latest or 012345678910.dkr.ecr.<region-name>.amazonaws.com/<repository-name>@sha256:94afd1f2e64d908bc90dbca0035a5b567EXAMPLE.
                 Images in official repositories on Docker Hub use a single name (for example, ubuntu or mongo).
                 Images in other repositories on Docker Hub are qualified with an organization name (for example, amazon/amazon-ecs-agent).
  ContainerPort:
    Type: Number
    Default: 80
    Description: What port number the application inside the docker container is binding to


  # --- CPU -------------------------------------------------------------------------
  # 256 (.25 vCPU) - Available memory values: 0.5GB, 1GB, 2GB
  # 512 (.5 vCPU) - Available memory values: 1GB, 2GB, 3GB, 4GB
  # 1024 (1 vCPU) - Available memory values: 2GB, 3GB, 4GB, 5GB, 6GB, 7GB, 8GB
  # 2048 (2 vCPU) - Available memory values: Between 4GB and 16GB in 1GB increments
  # 4096 (4 vCPU) - Available memory values: Between 8GB and 30GB in 1GB increments 
  #
  # A task like nginx is lean and needs only 0.25 vCPU while spring boot may  need more potentially 2vCPU.
  #
  ContainerCpu:
    Type: Number
    Default: 256
    Description: How much CPU to give the container. 1024 is 1 vCPU
  
  # --- MEMORY ----------------------------------------------------------------------
  # The hard limit of memory (in MiB) to present to the task. It can be expressed as an integer using MiB, for example 1024, 
  # or as a string using GB, for example 1GB or 1 GB, in a task definition. When the task definition is registered, a GB value
  # is converted to an integer indicating the MiB.
  #
  # 512 (0.5 GB), 1024 (1 GB), 2048 (2 GB) - Available cpu values: 256 (.25 vCPU)
  # 1024 (1 GB), 2048 (2 GB), 3072 (3 GB), 4096 (4 GB) - Available cpu values: 512 (.5 vCPU)
  # 2048 (2 GB), 3072 (3 GB), 4096 (4GB), 5120 (5 GB), 6144 (6 GB), 7168 (7 GB), 8192 (8 GB) - Available cpu values: 1024 (1 vCPU)
  # Between 4GB and 16GB in 1GB increments - Available cpu values: 2048 (2 vCPU)
  # Between 8GB and 30GB in 1GB increments - Available cpu values: 4096 (4 vCPU)  
  # 
  # The Docker daemon reserves a minimum of 4 MiB of memory for a container, so you should not specify fewer than 4 MiB of 
  # memory for your containers.
  #
  # A task like nginx is lean and needs only 512MB however SpringBoot may need more , potentially 4 GB
  #
  ContainerMemory:
    Type: Number
    Default: 512
    Description: How much memory in megabytes to give the container
  
  Path:
    Type: String
    Default: "*"
    Description: A path on the public load balancer that this service
                 should be connected to. Use * to send all load balancer
                 traffic to this service.
  Priority:
    Type: Number
    Default: 1
    Description: The priority for the routing rule added to the load balancer.
                 This only applies if your have multiple services which have been
                 assigned to different paths on the load balancer.
  DesiredCount:
    Type: Number
    Default: 2
    Description: How many copies of the service task to run
  
  Role:
    Type: String
    Default: ""
    Description: (Optional) An IAM role to give the service's containers if the code within needs to
                 access other AWS resources like S3 buckets, DynamoDB tables, etc

Conditions:
  HasCustomRole: !Not [ !Equals [!Ref 'Role', ''] ]

Resources:

  LogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Join ['', [/ecs/, !Ref ServiceName, TaskDefinition, !Ref StackStage ]]


  # The task definition. This is a simple metadata description of what
  # container to run, and what resource requirements it has.
  TaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: !Ref 'ServiceName'
      Cpu: !Ref 'ContainerCpu'
      Memory: !Ref 'ContainerMemory'
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      ExecutionRoleArn:
        Fn::ImportValue:
          !Join [':', [!Ref 'VpcStackName', 'ECSTaskExecutionRole']]
      TaskRoleArn:
        Fn::If:
          - 'HasCustomRole'
          - !Ref 'Role'
          - !Ref "AWS::NoValue"
      ContainerDefinitions:
        # This is a single container
        - Name: !Ref 'ServiceName'
          # The number of cpu units reserved for the container. - How much CPU to give the container.
          Cpu: !Ref 'ContainerCpu'
          Memory: !Ref 'ContainerMemory'
          # The image used to start a container. 
          Image: !Ref 'ImageUrl'
          PortMappings:
            # This parameter maps to PortBindings in the Create a container section of the Docker Remote API and the --publish option to docker run. 
            - ContainerPort: !Ref 'ContainerPort'
          # Send logs to CloudWatch Logs
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-region: !Ref AWS::Region
              awslogs-group: !Ref LogGroup
              awslogs-stream-prefix: ecs


  # The service. The service is a resource which allows you to run multiple
  # copies of a type of task, and gather up their logs and metrics, as well
  # as monitor the number of running tasks and replace any that have crashed
  Service:
    Type: AWS::ECS::Service
    DependsOn: LoadBalancerRule
    Properties:
      ServiceName: !Ref 'ServiceName'
      Cluster:
        Fn::ImportValue:
          !Join [':', [!Ref 'VpcStackName', 'ClusterName']]
      LaunchType: FARGATE
      DeploymentConfiguration:
        MaximumPercent: 200
        MinimumHealthyPercent: 75
      DesiredCount: !Ref 'DesiredCount'
      # This may need to be adjusted if the container takes a while to start up
      HealthCheckGracePeriodSeconds: 30
      NetworkConfiguration:
        AwsvpcConfiguration:
          # Whether the task's elastic network interface receives a public IP address. The default value is DISABLED.
          # Change to DISABLED if you're using private subnets that have access to a NAT gateway
          AssignPublicIp: ENABLED
          # There is a limit of 5 security groups that can be specified
          SecurityGroups:
            - Fn::ImportValue:
                !Join [':', [!Ref 'VpcStackName', 'FargateContainerSecurityGroup']]
          # The subnets associated with the task or service. There is a limit of 16.
          Subnets:
            - Fn::ImportValue:
                !Join [':', [!Ref 'VpcStackName', 'PublicSubnetOne']]
            - Fn::ImportValue:
                !Join [':', [!Ref 'VpcStackName', 'PublicSubnetTwo']]
      TaskDefinition: !Ref 'TaskDefinition'
      LoadBalancers:
        - ContainerName: !Ref 'ServiceName'
          ContainerPort: !Ref 'ContainerPort'
          TargetGroupArn: !Ref 'TargetGroup'

  # A target group. This is used for keeping track of all the tasks, and
  # what IP addresses / port numbers they have. You can query it yourself,
  # to use the addresses yourself, but most often this target group is just
  # connected to an application load balancer, or network load balancer, so
  # it can automatically distribute traffic across all the targets.
  TargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      # The name of the target group.
      # This name must be unique per region per account, can have a maximum of 32 characters, must contain only alphanumeric characters or hyphens, and must not begin or end with a hyphen.
      Name: !Sub ${StackName}-TG-${StackStage}
      HealthCheckIntervalSeconds: 6
      HealthCheckPath: /
      HealthCheckProtocol: HTTP
      HealthCheckTimeoutSeconds: 5
      HealthyThresholdCount: 2
      TargetType: ip
      Port: !Ref 'ContainerPort'
      Protocol: HTTP
      UnhealthyThresholdCount: 2
      VpcId:
        Fn::ImportValue:
          !Join [':', [!Ref 'VpcStackName', 'VPCId']]
      Tags: 
      - Key: Name 
        Value: !Sub ${StackName}-${ServiceName}-${StackStage}
      - Key: scripted
        Value: true
      - Key: ServiceName
        Value: !Ref ServiceName
      - Key: VpcStackName
        Value: !Ref VpcStackName
      - Key: StackName
        Value: !Ref StackName
      - Key: stage
        Value: !Ref StackStage



  # Create a rule on the load balancer for routing traffic to the target group
  LoadBalancerRule:
    Type: AWS::ElasticLoadBalancingV2::ListenerRule
    Properties:
      Actions:
        - TargetGroupArn: !Ref 'TargetGroup'
          Type: 'forward'
      Conditions:
        - Field: path-pattern
          Values: [!Ref 'Path']
      ListenerArn:
        Fn::ImportValue:
          !Join [':', [!Ref 'VpcStackName', 'PublicListener']]
      Priority: !Ref 'Priority'
      
# ------------------------------------------------------------------------------------------------------
# These are the values output by the CloudFormation template. Be careful
# about changing any of them, because of them are exported with specific
# names so that the other task related CF templates can use them.
# ------------------------------------------------------------------------------------------------------
Outputs:
  ImageUrl:
    Description: The container image 
    Value: !Ref 'ImageUrl'
    Export:
      Name: !Join [ ':', [ !Ref 'AWS::StackName', 'ImageUrl' ] ]  

  VpcStackName:
    Description: The name of the parent Fargate networking stack that hosts this stack.
    Value: !Ref 'VpcStackName'
    Export:
      Name: !Join [ ':', [ !Ref 'AWS::StackName', 'VpcStackName' ] ]  
  
  ServiceName:
    Description: A name for the service in the cluster.
    Value: !Ref 'ServiceName'
    Export:
      Name: !Join [ ':', [ !Ref 'AWS::StackName', 'ServiceName' ] ]  
  
  
  LogGroup:
    Description: A name for the service in the cluster.
    Value: !Ref 'LogGroup'
    Export:
      Name: !Join [ ':', [ !Ref 'AWS::StackName', 'LogGroup' ] ]  
        