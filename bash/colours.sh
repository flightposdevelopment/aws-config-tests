#!/bin/bash
# Colours : https://misc.flogisoft.com/bash/tip_colors_and_formatting
NC="\e[39m"
BLUE="\e[34m"
RED="\e[91m"
YELLOW="\e[93m"
GREEN="\e[32m"
LGRAY="\e[37m"
CYAN="\e[36m"
NORM=" \e[0m"
BOLD="\e[1m"
DIM=" \e[2m"
