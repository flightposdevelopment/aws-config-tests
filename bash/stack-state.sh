#!/bin/bash
. ./colours.sh

# --- PARAMS ----------------------------------------------
if [ -z "$1" ]
then
      echo "usage stackName - Define the stackName"
      exit 0 
fi

# --- VARIABLES ----------------------------------------------
export stackStage=dev
export stackName=$1-$stackStage

# --------------------------------------------------------------------------------------------------------------------------------
echo "---STATE of '$1' ---"
export stackState=$(aws cloudformation describe-stacks --stack-name $stackName --query "Stacks[0].StackStatus" --output text)
echo $stackState
