#!/bin/bash
DIR=$(dirname "$0")

. ${DIR}/colours.sh

# ---------------------------------------------------------------------------------
usage()
{
echo -e ${BOLD}${BLUE} +++ ECRPUSH +++${NC}
cat << EOF
usage: $0 options

Fetches the properties from the aws creds file.
OPTIONS:
   
   -q   Quiet
   -h   Usage
   -i	ImageUrl - The url of a docker image that contains the application process that
     	           will handle the traffic for this service.
   -c	Container distribution
   -n	stackNameBase (nginx etc) 
   -s	Stage (Dev etc)
   -t	Template (Dev etc)
   -v	vpc Name prefix (fargate etc) Helps find the 'VpcStackName' do deploy into.
   
EOF
}

echo ==================================================================================
echo -e ${YELLOW}STACK CREATE or UPDATE${NC} 
echo ==================================================================================

# --- PARAMS ----------------------------------------------
stackStage=dev
stackType=ecs
stackNameBase=nginx
ImageUrl=nginx
vpcNamePrefix=fargate
ContainerPort=80

CONTAINER_ACCOUNT="mcnought"
CONTAINER_NAME="ecs-microservice"
CONTAINER_TAG="0.0.1"
PROFILE_NAME_CMD=""
verbose=1
while getopts "h?qcn:s:v:t:i:p:" opt; do

    case "$opt" in
    h|\?)
        usage
        exit
        ;;
    q)  verbose=0
        ;;
    c)  stackType=ecs
    	echo "Deploying a container"
        ;;
    i)  ImageUrl=$OPTARG
    	echo "Using ImageUrl=$ImageUrl"
        ;;
    n)  stackNameBase=$OPTARG
    	echo "Using stackNameBase=$stackNameBase"
        ;;
    p)  ContainerPort=$OPTARG
    	echo "Using ContainerPort=$ContainerPort"
        ;;
    s)  stackStage=$OPTARG
    	echo "Using stackStage=$stackStage"
        ;;        
    t)  templateName=$OPTARG
    	echo "Using templateName=$templateName"
        ;;        
    v)  vpcNamePrefix=$OPTARG
    	echo "Using vpcNamePrefix=$vpcNamePrefix"
        ;;        

    esac
done


# --- VARIABLES ----------------------------------------------

export stackName=${stackNameBase}-${stackType}
export VpcStackName=${vpcNamePrefix}-vpc-${stackStage}
export scriptsPath=${DIR}/..

# --- PARAMS ----------------------------------------------
if [ -z "$stackType" ]
then
      echo -e "\n${BOLD}${RED}usage - Define the stackType${NC}\n"
        usage
        exit
fi
if [ -z "$stackNameBase" ]
then
      echo -e "\n${BOLD}${RED}usage - Define the stackNameBase${NC}\n"
        usage
        exit
fi
if [ -z "$templateName" ]
then
      echo -e "\n${BOLD}${RED}usage - Define the templateName${NC}\n"
        usage
        exit
fi


echo -e ${DIM}
echo -e "\t${YELLOW}DEPLOY${NC} :: ${stackName}-${stackStage}"

echo -e "\tBaseName\t$stackNameBase"
echo -e "\tStage\t$stackStage"
echo -e "\tType\t$stackType"
echo -e "\tUsing\t$templateName${NC}"
echo


. ${DIR}/stack-cloudformation.sh

echo ==================================================================================
. ${DIR}/stack-exists.sh 
echo =================================================================================



















exit 0





#
# Is it existing
if [ ! -z "$stackState" ] 
then
	if [ $stackState == 'DELETE_IN_PROGRESS' ]
	then
		echo $stackFullName is in state=$stackState
		exit -1 
	fi
	if [ $stackState == 'ROLLBACK_COMPLETE' ]
	then
		#
		# Remove
		#
		echo $stackFullName is in state=$stackState
		echo delete stack
		aws cloudformation delete-stack --stack-name $stackFullName
		unset stackState
	fi
	if [ $stackState == 'CREATE_COMPLETE' ] || [ $stackState == 'UPDATE_COMPLETE' ]
	then
		#
		# Update
		#
		echo ---UPDATE---
		echo VpcName=$stackFullName	
		echo StackStage=$stackStage	
		echo StackName=$stackName	
		aws cloudformation update-stack --template-body file://$templateFile 						\
										--stack-name $stackFullName  									\
										--parameters ParameterKey=StackName,ParameterValue=$stackName	\
													 ParameterKey=StackStage,ParameterValue=$stackStage	\
													 ParameterKey=VpcName,ParameterValue=$stackFullName
									             
		export stackState=$(aws cloudformation describe-stacks --stack-name $stackFullName --query "Stacks[0].StackStatus" --output text)
		
		while [ $stackState == 'UPDATE_IN_PROGRESS' ]
		do
			echo $stackState waiting...
			sleep 2 # Waits 2 second.
			export stackState=$(aws cloudformation describe-stacks --stack-name $stackFullName --query "Stacks[0].StackStatus" --output text)
		done
		
	fi
else
	# 
	# Stack state not defined
	#

	echo 	Create stack...

	#
	# Create
	#
	echo ---CREATE---
	echo VpcName=$stackFullName	
	echo StackStage=$stackStage	
	echo StackName=$stackName	
	aws cloudformation create-stack --template-body file://$templateFile 						\
									--stack-name $stackFullName  									\
									--parameters ParameterKey=StackName,ParameterValue=$stackName	\
												 ParameterKey=StackStage,ParameterValue=$stackStage	\
												 ParameterKey=VpcName,ParameterValue=$stackFullName

	export stackState=$(aws cloudformation describe-stacks --stack-name $stackFullName --query "Stacks[0].StackStatus" --output text)
	
	while [ $stackState == 'CREATE_IN_PROGRESS' ]
	do
		echo $stackState waiting...
		sleep 1 # Waits 1 second.
		export stackState=$(aws cloudformation describe-stacks --stack-name $stackFullName --query "Stacks[0].StackStatus" --output text)
	done
	
fi
echo "---STATE of '$stackFullName' == $stackState ---"									
echo ---------------------------------------------------------------------
echo -e ${BLUE}${BOLD}WHAT NOW EXISTS${NC} :: ${stackFullName}

echo -e ${CYAN}VPCs${NC}
aws cloudformation list-stack-resources --stack-name $stackFullName --query "StackResourceSummaries[?ResourceType=='AWS::EC2::VPC'].{LogicalId:LogicalResourceId,PhysicalId:PhysicalResourceId}" --output text

echo -e ${CYAN}SUBNETS${NC}
aws cloudformation list-stack-resources --stack-name $stackFullName --query "StackResourceSummaries[?ResourceType=='AWS::EC2::Subnet'].{LogicalId:LogicalResourceId,PhysicalId:PhysicalResourceId}" --output text

echo -e ${DIM}done.





