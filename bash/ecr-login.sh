#!/bin/bash

DIR=${PWD}/$(dirname $BASH_SOURCE)

. ${DIR}/colours.sh


# ---------------------------------------------------------------------------------
usage()
{
echo -e ${BOLD}${BLUE} +++ ECRPUSH +++${NC}
cat << EOF
usage: $0 [-p name] [-q]

Fetches the properties from the aws creds file.
OPTIONS:
   
   -p   User profile
   -q   Quiet
   -h   Usage

   
EOF
}

echo
echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
echo -e ${BOLD}${BLUE} +++ ECRLOGIN +++${NC}
echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
echo


# --- PARAMS ----------------------------------------------
CONTAINER_ACCOUNT=
CONTAINER_NAME=
CONTAINER_TAG=
PROFILE_NAME_CMD=""
verbose=1
while getopts "h?qp:" opt; do

    case "$opt" in
    h|\?)
        usage
        read -p "Press enter to continue"
        return
        ;;
    q)  verbose=0
        ;;
    p)  PROFILE_NAME=$OPTARG
    	PROFILE_NAME_CMD="-p $PROFILE_NAME"
    	echo "Using PROFILE_NAME=$PROFILE_NAME"
        ;;
    esac
done

# --- PARAMS ----------------------------------------------
if [ -z "$AWS_ACCOUNT_ID" ] || [ -z "$AWS_REGION" ];
then

	echo Get Aws creds 
	
	eval $(${DIR}/awscreds.sh -q ${PROFILE_NAME_CMD})
	
	if [ $verbose = 1 ];
	then
		echo -e "\tAWS_ACCOUNT_ID=${AWS_ACCOUNT_ID}"
		echo -e "\tAWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}"
		echo -e "\tAWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}"
		echo -e "\tAWS_REGION=${AWS_REGION}"
	fi

fi


# Validate
if [ -z "$AWS_ACCOUNT_ID" ] ; then echo -e "${RED}define AWS_ACCOUNT_ID${NC}\n" ;  return ; fi
if [ -z "$AWS_REGION" ] ; then echo -e "${RED}define AWS_REGION${NC}\n" ;  return ; fi

export DOCKER_REGISTRY=${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com

echo -e "Checking registry ${DOCKER_REGISTRY}${LGRAY}"

echo -e "AWS ECR Login...${LGRAY}"
$(aws ecr get-login --no-include-email) 
echo


