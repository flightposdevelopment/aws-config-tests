#!/bin/bash
DIR=$(dirname "$0")
. ${DIR}/colours.sh

#
# Find the State of the stack
#

# --- PARAMS ----------------------------------------------
if [ -z "$stackName" ]
then
      echo "usage stackName - Define the stackName"
      exit 0 
fi
if [ -z "$stackNameBase" ]
then
      echo "usage stackNameBase - Define the stackNameBase"
      exit 0 
fi
if [ -z "$stackStage" ]
then
      echo "usage stackStage - Define the stackStage"
      exit 0 
fi
if [ -z "$templateName" ]
then
      echo "usage templateName - Define the templateName"
      exit 0 
fi
if [ -z "$stackType" ]
then
      echo "usage stackType - Define the stackType"
      exit 0 
fi
if [ -z "$scriptsPath" ]
then
      echo "usage scriptsPath - Define the scriptsPath"
      exit 0 
fi

# --- VARIABLES ----------------------------------------------
export templateFile=${scriptsPath}/templates/${templateName}.yml
export stackFullName=${stackName}-${stackStage}

# Tuned for SpringBoot -- FAT
ContainerCpu=2048
ContainerMemory=4096 


echo -e "\t${DIM}VpcStackName${NORM}\t VpcStackName=$VpcStackName"


echo -e "\t${DIM}templateFile${NORM}\t templateFile=$templateFile"
echo -e "\t${DIM}stackStage${NORM}\t stackStage=$stackStage"
echo -e "\t${DIM}stackName${NORM}\t stackName=$stackName"
echo


export stackNames=$(aws cloudformation list-stacks  \
	--stack-status-filter \
		UPDATE_ROLLBACK_COMPLETE \
		UPDATE_COMPLETE \
		ROLLBACK_COMPLETE \
		DELETE_IN_PROGRESS \
		CREATE_IN_PROGRESS \
		CREATE_COMPLETE \
		--query "StackSummaries[*].StackName" --output json) 

#
# Just describe the current stack state.
#		
#echo $stackNames
if [[ $stackNames != *\"${stackFullName}\"* ]]; then
	export stackState="NOT_FOUND"
else
	export stackState=$(aws cloudformation describe-stacks --stack-name $stackFullName --query "Stacks[0].StackStatus" --output text)
fi
echo "Current Stack State - [${stackState}]"

#
# Now either create or update
#
if [ $stackState == 'ROLLBACK_COMPLETE' ] 
then
	echo ================================================================================================
	echo -e ${RED}FAILED${NC} :: $stackType - ${stackFullName} 
	echo ================================================================================================
	echo 
	echo Stack is in ROLLBACK_COMPLETE state and can not be updated.
	echo Delete the current stack through the console.
	echo 
	exit -1 
elif [ $stackState == 'CREATE_COMPLETE' ] \
|| [ $stackState == 'UPDATE_COMPLETE' ] \
|| [ $stackState == 'UPDATE_ROLLBACK_COMPLETE' ]
then
	echo ================================================================================================
	echo -e ${RED}UPDATE${NC} :: $stackType - ${stackFullName} using ${templateFile}
	echo ================================================================================================
	
	if [ $stackType == "vpc" ]; then
		aws cloudformation update-stack --template-body file://$templateFile 						\
										--stack-name $stackFullName  									\
										--capabilities CAPABILITY_NAMED_IAM							\
										--parameters ParameterKey=StackName,ParameterValue=$stackName	\
													 ParameterKey=StackStage,ParameterValue=$stackStage	
	else
	
		# ------------------------------------------
		# Create a stack for ECS 
		
		if [ -z "$ImageUrl" ]
		then
		      echo "usage ImageUrl - Define the Image Url"
		      exit 0 
		fi	
		
		aws cloudformation update-stack --template-body file://$templateFile 						\
										--stack-name $stackFullName  									\
										--capabilities CAPABILITY_NAMED_IAM							\
										--parameters ParameterKey=StackName,ParameterValue=$stackName	\
													 ParameterKey=StackStage,ParameterValue=$stackStage	\
													 ParameterKey=VpcStackName,ParameterValue=$VpcStackName \
													 ParameterKey=ServiceName,ParameterValue=$stackNameBase	\
													 ParameterKey=ImageUrl,ParameterValue=$ImageUrl	\
													 ParameterKey=ContainerPort,ParameterValue=$ContainerPort	\
													 ParameterKey=ContainerCpu,ParameterValue=$ContainerCpu	\
													 ParameterKey=ContainerMemory,ParameterValue=$ContainerMemory	

	fi	

	sleep 1 # Waits 1 second.
	export stackState=$(aws cloudformation describe-stacks --stack-name $stackFullName --query "Stacks[0].StackStatus" --output text)
	
	
elif [ $stackState != 'CREATE_IN_PROGRESS' ] \
&& [ $stackState != 'CREATE_COMPLETE' ]
then
	echo =============================================================================================
	echo -e ${GREEN}CREATING${NC} :: $stackType - ${stackFullName} using ${templateFile}
	echo =============================================================================================

	
	if [ $stackType == "vpc" ]; then
	
		aws cloudformation create-stack --template-body file://$templateFile 						\
										--stack-name $stackFullName  									\
										--capabilities CAPABILITY_NAMED_IAM							\
										--parameters ParameterKey=StackName,ParameterValue=$stackName	\
													 ParameterKey=StackStage,ParameterValue=$stackStage	
	else
	

		# ------------------------------------------
		# Create a stack for ECS 
		
		if [ -z "$ImageUrl" ]
		then
		      echo -e "\nusage ImageUrl - Define the Image Url\n"
		      exit 0 
		fi	
		if [ -z "$ContainerPort" ]
		then
		      echo -e "\nusage ContainerPort - Define the Port the application is waiting on.\n"
		      exit 0 
		fi	

		echo -e ${GREEN}CREATING${NC} :: using ${ImageUrl}

#echo QUIT
#exit 0
	
		aws cloudformation create-stack --template-body file://$templateFile 						\
										--stack-name $stackFullName  									\
										--capabilities CAPABILITY_NAMED_IAM							\
										--parameters ParameterKey=StackName,ParameterValue=$stackName	\
													 ParameterKey=StackStage,ParameterValue=$stackStage	\
													 ParameterKey=VpcStackName,ParameterValue=$VpcStackName \
													 ParameterKey=ServiceName,ParameterValue=$stackNameBase	\
													 ParameterKey=ImageUrl,ParameterValue=$ImageUrl	\
													 ParameterKey=ContainerPort,ParameterValue=$ContainerPort	\
													 ParameterKey=ContainerCpu,ParameterValue=$ContainerCpu	\
													 ParameterKey=ContainerMemory,ParameterValue=$ContainerMemory	
													 
	fi		
	
	sleep 1 # Waits 1 second.
	export stackState=$(aws cloudformation describe-stacks --stack-name $stackFullName --query "Stacks[0].StackStatus" --output text)
	
fi

# --------- WAIT ---------------------------------------------------
echo
echo "Current Stack State - [${stackState}]"
export ticker="\\"
while [ $stackState == 'CREATE_IN_PROGRESS' ] \
  ||  [ $stackState == 'UPDATE_IN_PROGRESS' ] \
  ||  [ $stackState == 'UPDATE_ROLLBACK_IN_PROGRESS' ]	  
do
	#echo [${ticker}]

	if [ $ticker = "\\" ]
	then
		export ticker="|"
	elif [ $ticker = "|" ]
	then 
		export ticker="/"
	elif [ $ticker = "/" ]
	then 
		export ticker="-"
	else 
		export ticker="\\"
	fi
	

	echo -ne "${GREEN}FORMING${NC} :: ${stackFullName} - $stackState waiting $ticker \r"
	sleep 1 # Waits 1 second.
	export stackState=$(aws cloudformation describe-stacks --stack-name $stackFullName --query "Stacks[0].StackStatus" --output text)
done

 

