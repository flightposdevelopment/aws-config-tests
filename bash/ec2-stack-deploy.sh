#!/bin/bash

export stackName=$1
export scriptsPath=$2
export DockerName=$3
export DockerImage=$4
export DockerVersion=$5
export DockerUid=$6
export DockerPwd=$7
export BitbucketUid=$8
export BitbucketPwd=$9

export templateFile=${scriptsPath}/templates/single-instance.yml

echo ==================================================================================
echo DEPLOY :: $stackName
echo           Using $templateFile
echo           DockerName= $DockerName
echo           DockerImage= $DockerImage
echo           DockerUid= $DockerUid
echo           DockerPwd= $DockerPwd
echo           DockerVersion= $DockerVersion
echo           BitbucketPwd= $BitbucketPwd
echo           BitbucketUid= $BitbucketUid
echo ==================================================================================

#
# Delete Stack
#
#  delete-stack
#--stack-name <value>

#
# Key Pairs
#
export ec2KeyName=$(aws ec2 describe-key-pairs | jq -r .KeyPairs[].KeyName)


#
# State
#
echo ---STATE---
export stackState=$(aws cloudformation describe-stacks --stack-name $stackName --query "Stacks[0].StackStatus" --output text)

#
# Is it existing
if [ ! -z "$stackState" ] 
then
	echo $stackName is in state=$stackState
	if [ $stackState == 'DELETE_IN_PROGRESS' ]
	then
		exit -1 
	fi
	if [ $stackState == 'ROLLBACK_COMPLETE' ]
	then
		#
		# Remove
		#
		echo delete stack
		aws cloudformation delete-stack --stack-name $stackName
		unset stackState
	fi
	if [ $stackState == 'CREATE_COMPLETE' ]
	then
		#
		# Update
		#
		echo ---UPDATE---
		aws cloudformation update-stack --template-body file://$templateFile \
										--stack-name $stackName \
										--parameters ParameterKey=KeyName,ParameterValue=$ec2KeyName	\
										             ParameterKey=DockerName,ParameterValue=$DockerName	\
										             ParameterKey=DockerImage,ParameterValue=$DockerImage	\
										             ParameterKey=DockerUid,ParameterValue=$DockerUid	\
										             ParameterKey=DockerPwd,ParameterValue=$DockerPwd	\
										             ParameterKey=DockerVersion,ParameterValue=$DockerVersion	\
										             ParameterKey=BitbucketUid,ParameterValue=$BitbucketUid	\
										             ParameterKey=BitbucketPwd,ParameterValue=$BitbucketPwd	
										             
										             
	fi

fi

#
# Create if not existing
if [ -z "$stackState" ] 
then
	echo 	Create stack...

	echo 		Using ec2KeyName is $ec2KeyName

	#
	# Create
	#
	echo ---CREATE---
	aws cloudformation create-stack --template-body file://$templateFile \
									--stack-name $stackName \
									--parameters ParameterKey=KeyName,ParameterValue=$ec2KeyName	\
										             ParameterKey=DockerName,ParameterValue=$DockerName	\
										             ParameterKey=DockerImage,ParameterValue=$DockerImage	\
										             ParameterKey=DockerUid,ParameterValue=$DockerUid	\
										             ParameterKey=DockerPwd,ParameterValue=$DockerPwd	\
										             ParameterKey=DockerVersion,ParameterValue=$DockerVersion	\
										             ParameterKey=BitbucketUid,ParameterValue=$BitbucketUid	\
										             ParameterKey=BitbucketPwd,ParameterValue=$BitbucketPwd	
	
fi






