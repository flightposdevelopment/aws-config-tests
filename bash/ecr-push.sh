#!/bin/bash

DIR=$(dirname "$0")

. ${DIR}/colours.sh


# ---------------------------------------------------------------------------------
usage()
{
echo -e ${BOLD}${BLUE} +++ ECRPUSH +++${NC}
cat << EOF
usage: $0 [-p name] [-q]

Fetches the properties from the aws creds file.
OPTIONS:
   
   -p   User profile
   -q   Quiet
   -h   Usage
   -a	Container Account
   -c	Container name
   -t	Container tag
   
EOF
}

echo
echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
echo -e ${BOLD}${BLUE} +++ ECRPUSH +++${NC}
echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
echo


# --- PARAMS ----------------------------------------------
CONTAINER_ACCOUNT=
CONTAINER_NAME=
CONTAINER_TAG=
PROFILE_NAME_CMD=""
verbose=1
while getopts "h?qa:c:t:p:" opt; do

    case "$opt" in
    h|\?)
        usage
        exit
        ;;
    q)  verbose=0
        ;;
    a)  CONTAINER_ACCOUNT=$OPTARG
    	echo "Using CONTAINER_ACCOUNT=$CONTAINER_ACCOUNT"
        ;;
    c)  CONTAINER_NAME=$OPTARG
    	echo "Using CONTAINER_NAME=$CONTAINER_NAME"
        ;;
    p)  PROFILE_NAME=$OPTARG
    	PROFILE_NAME_CMD="-p $PROFILE_NAME"
    	echo "Using PROFILE_NAME=$PROFILE_NAME"
        ;;
    t)  CONTAINER_TAG=$OPTARG
    	echo "Using PROFILE_NAME=$PROFILE_NAME"
        ;;
    esac
done

if [ -z "$CONTAINER_ACCOUNT" ] ; then echo -e "${RED}define CONTAINER_ACCOUNT${NC}\n" ; exit -1 ; fi
if [ -z "$CONTAINER_NAME" ] ; then echo -e "${RED}define CONTAINER_NAME${NC}\n" ; exit -1 ; fi
if [ -z "$CONTAINER_TAG" ] ; then echo -e "${RED}define CONTAINER_TAG${NC}\n" ; exit -1 ; fi

# --- PARAMS ----------------------------------------------
if [ -z "$AWS_ACCOUNT_ID" ] || [ -z "$AWS_REGION" ];
then

	echo Get Aws creds
	
	eval $(${DIR}/awscreds.sh -q ${PROFILE_NAME_CMD})
	
	if [ $verbose = 1 ];
	then
		echo -e "\tAWS_ACCOUNT_ID=${AWS_ACCOUNT_ID}"
		echo -e "\tAWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}"
		echo -e "\tAWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}"
		echo -e "\tAWS_REGION=${AWS_REGION}"
	fi

fi


# Validate
if [ -z "$AWS_ACCOUNT_ID" ]
then
	echo No $AWS_ACCOUNT_ID
	exit -1 
fi
if [ -z "$AWS_REGION" ]
then
	echo No $AWS_REGION
	exit -1 
fi

#---------------------------------------------------------------------------
# Check for container
container_line=$(docker images | grep ${CONTAINER_ACCOUNT} | grep ${CONTAINER_NAME} | grep ${CONTAINER_TAG})
if [ -z "$AWS_ACCOUNT_ID" ]
then
	echo No container ${CONTAINER_ACCOUNT}/${CONTAINER_NAME}:${CONTAINER_TAG}
	exit -1 
fi

echo
echo -e ${BOLD}${BLUE} +++ Deploying +++${NC}
echo -e "\t${NC}Deploying to ${YELLOW}${AWS_ACCOUNT_ID}${NC}"
echo -e "\t${NC}from ${YELLOW}${CONTAINER_ACCOUNT}/${CONTAINER_NAME}:${CONTAINER_TAG}${NC} to ${YELLOW}${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/${CONTAINER_NAME}:${CONTAINER_TAG}${NC}"

echo -e "\tTagging first..."
docker tag ${CONTAINER_ACCOUNT}/${CONTAINER_NAME}:${CONTAINER_TAG} ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/${CONTAINER_NAME}:${CONTAINER_TAG}



echo -e "\tAWS ECR Login...${LGRAY}"
$(aws ecr get-login --no-include-email) 
echo
echo -e "\tPushing ..."
docker push ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/${CONTAINER_NAME}

echo 
echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
exit 0


#echo
#echo -e ${BOLD}${BLUE} +++ Labeling +++${NC}
#MANIFEST=$(aws ecr batch-get-image --repository-name ${CONTAINER_NAME} --image-ids imageTag=latest --query 'images[].imageManifest' --output text)
#aws ecr put-image --repository-name ${CONTAINER_NAME} --image-tag ${CONTAINER_TAG} --image-manifest "$MANIFEST" > null

echo 
echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
LABELS=$(aws ecr describe-images --repository-name ${CONTAINER_NAME} --query 'imageDetails[].imageTags' --output text)
echo -e Lables in place : ${BOLD}${LABELS}

echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
echo done.

