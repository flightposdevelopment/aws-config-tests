  692  clear && ./stack-destroy.sh vpc fargate public-vpc
  693  clear && ./stack-delete.sh vpc fargate public-vpc

  Deploy the "fargate" VPC
  ```
  clear && ./stack-create.sh vpc fargate public-vpc
  ```
  
  Deploy NGNINX
  ```
  clear && ./stack-create.sh ecs ngnix public-subnet-public-loadbalancer fargate
  ```
  
  List all the clusters `aws ecs list-clusters`;
  Get the first cluster ARN `export clusterArn=$(aws ecs list-clusters --query "clusterArns[0]" --output text)`;
  To the the list of services `aws ecs list-services --cluster $clusterArn`;
  To the the list of tasks `aws ecs list-tasks --cluster $clusterArn`;
  To describe the service 'nginx' use: `aws ecs describe-services --cluster $clusterArn --services nginx`
  