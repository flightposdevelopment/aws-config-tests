#!/bin/bash
. ./colours.sh

# ---------------------------------------------------------------------------------
usage()
{
echo -e ${BOLD}${BLUE} +++ ECRPUSH +++${NC}
cat << EOF
usage: $0 options

Fetches the properties from the aws creds file.
OPTIONS:
   
   -q   	Quiet
   -h   	Usage
   -c		Container distribution
   -n	 	stackNameBase (nginx etc) Helps find the 'VpcStackName' do deploy into.
   -s	 	Stage (Dev etc)
   
EOF
}

echo ==================================================================================
echo -e ${YELLOW}STACK DELETE${NC} 
echo ==================================================================================

# --- VARIABLES ----------------------------------------------

# --- PARAMS ----------------------------------------------
stackStage=dev
stackType=ecs
stackNameBase=nginx
vpcNamePrefix=fargate

verbose=1
while getopts "h?qcn:s:" opt; do

    case "$opt" in
    h|\?)
        usage
        exit
        ;;
    q)  verbose=0
        ;;
    c)  stackType=ecs
    	echo "Deploying a container"
        ;;
    n)  stackNameBase=$OPTARG
    	echo "Using stackNameBase=$stackNameBase"
        ;;
    s)  stackStage=$OPTARG
    	echo "Using stackStage=$stackStage"
        ;;        
    t)  templateName=$OPTARG

    esac
done


# --- VARIABLES ----------------------------------------------
export stackName=${stackNameBase}-${stackType}


# --- PARAMS ----------------------------------------------
if [ -z "$stackName" ]
then
      echo "usage stackName - Define the stackName"
      exit 0 
fi
if [ -z "$stackType" ]
then
      echo "usage stackType - Define the stackType"
      exit 0 
fi

echo -e ${DIM}
echo -e "\t${YELLOW}DELETE${NC} :: ${stackName}-${stackStage}"
echo -e "\tStage\t$stackStage"
echo -e "\tType\t$stackType"
echo

#-------------------------------
. ./stack-exists.sh 
#-------------------------------


echo ==================================================================================
export stackFullName=${stackName}-${stackStage}
aws cloudformation delete-stack --stack-name ${stackFullName}
. ./stack-wait-for-delete.sh

echo ==================================================================================
. ./stack-exists.sh 
echo =================================================================================

exit 0 


export scriptsPath=..

echo ==================================================================================
echo DEPLOY :: $stackName
echo ==================================================================================

#
# State
#
echo STATE...
export stackState=$(aws cloudformation describe-stacks --stack-name $stackName --query "Stacks[0].StackStatus" --output text)
echo ---STATE of '$1' ---



#aws cloudformation delete-stack --stack-name single-instance
