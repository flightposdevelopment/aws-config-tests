#!/bin/bash
DIR=$(dirname "$0")
. ${DIR}/colours.sh

#
# Find the State of the stack
#

# --- PARAMS ----------------------------------------------
if [ -z "$stackName" ]
then
      echo "usage stackName - Define the stackName"
      exit 0 
fi
if [ -z "$stackStage" ]
then
      echo "usage stackStage - Define the stackStage"
      exit 0 
fi
# --- VARIABLES ----------------------------------------------
export stackFullName=${stackName}-${stackStage}

export stackNames=$(aws cloudformation list-stacks  --stack-status-filter UPDATE_ROLLBACK_COMPLETE UPDATE_COMPLETE DELETE_IN_PROGRESS CREATE_IN_PROGRESS CREATE_COMPLETE CREATE_COMPLETE --query "StackSummaries[*].StackName" --output json) 
#echo $stackNames
#echo $1

if [[ $stackNames != *\"${stackFullName}\"* ]]; then
	echo -e "${DIM}CHECK(EXIST)${NORM}\tThe stack ${BOLD}${stackFullName}${NORM}can not be found."
  	if [[ $1 != "NOT" ]]; then 
  		exit 1
  	fi
else
	echo -e ${DIM}${stackFullName} Exists${NC}
  	if [[ $1 == "NOT" ]]; then 
  		exit 1
  	fi
fi

