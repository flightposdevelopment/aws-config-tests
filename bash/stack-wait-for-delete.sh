#!/bin/bash
. ./colours.sh

#
# Wait for the Stack if it is deleting
#

# --- PARAMS ----------------------------------------------
if [ -z "$stackName" ]
then
      echo "usage stackName - Define the stackName"
      exit 0 
fi
if [ -z "$stackStage" ]
then
      echo "usage stackStage - Define the stackStage"
      exit 0 
fi
# --- VARIABLES ----------------------------------------------
export stackFullName=${stackName}-${stackStage}

#--- get state -------------
export stackNames=$(aws cloudformation list-stacks  --stack-status-filter DELETE_IN_PROGRESS CREATE_COMPLETE CREATE_COMPLETE --query "StackSummaries[*].StackName" --output json) 

#echo $stackNames

if [[ $stackNames == *\"${stackFullName}\"* ]]; then
  echo -e "\nThe stack ${BOLD}${stackFullName}${NORM}found.\n"
  
    #
	# State
	#
	export stackState=$(aws cloudformation describe-stacks --stack-name $stackFullName --query "Stacks[0].StackStatus" --output text)
	if test ! -z "$stackState" 
	then 
		export ticker="\\"
		while [ $stackState == 'DELETE_IN_PROGRESS' ]
		do
			#echo [${ticker}]
		
			if [ $ticker = "\\" ]
			then
				export ticker="|"
			elif [ $ticker = "|" ]
			then 
				export ticker="/"
			elif [ $ticker = "/" ]
			then 
				export ticker="-"
			else 
				export ticker="\\"
			fi
			
		
			echo -ne "${YELLOW}DELETING${NC} :: ${stackName} - $stackState waiting $ticker \r"
			sleep 1 # Waits 1 second.
			export stackNames=$(aws cloudformation list-stacks  --stack-status-filter DELETE_IN_PROGRESS CREATE_IN_PROGRESS CREATE_COMPLETE CREATE_COMPLETE --query "StackSummaries[*].StackName" --output json) 
			if [[ $stackNames != *\"${stackFullName}\"* ]]; then
				echo -ne "${YELLOW}DELETING${NC} :: ${stackFullName} - DELETED $ticker \r"
				export stackState="DELETED"
			else
				export stackState=$(aws cloudformation describe-stacks --stack-name $stackFullName --query "Stacks[0].StackStatus" --output text)
			fi			
			
		done  
	fi
  
else
  echo -e "${DIM}CHECK(DELETE_IN_PROGRESS)${NORM}\tThe stack ${BOLD}${stackFullName}${NORM}can not be found."
fi



