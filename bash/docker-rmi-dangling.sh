#!/bin/bash -f
echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
echo "Removing dangling docker images ie. <NONE>"
echo
docker rmi $(docker images -f 'dangling=true' -q)