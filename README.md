# aws-config-tests

## Get AWS Creds
Simply run `./src/main/scripts/bash/get-aws-profile.sh`
to get `AWS_ACCESS_KEY_ID` etc.

## VPC Creation

These are scripts to deploys a VPC, with a pair of public and private subnets spread 
across two Availability Zones. 
Run this scenario if you want to run a public-facing web application, while maintaining back-end servers that aren't publicly accessible.
    
The instances in the public subnet can send outbound traffic directly to the Internet, whereas the instances in the private subnet can't. 
Instead, the instances in the private subnet can access the Internet by using a network address translation (NAT) gateway that resides in 
the public subnet.
    https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Scenario2.html
    
### To run
```
	cd ./src/main/scripts/bash
	./vpc-stack-deploy.sh my-stack
```
